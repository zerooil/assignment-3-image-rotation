#include "image.h"
#include <stdlib.h>

struct image create_image(const uint64_t width, const uint64_t height) {
  struct image img = {.width = width, .height = height};
  img.data =
      (struct pixel *)malloc(img.width * img.height * sizeof(struct pixel));
  if (img.data==NULL)
      return (struct image){
        .width = 0,
        .height = 0,
        .data = NULL
      };
  return img;
}
