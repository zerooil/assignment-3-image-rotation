#include "image.h"
#define MINIMUM_ANGLE 90
#define UPSIDE_DOWN_ANGLE (MINIMUM_ANGLE*2)
#define BACK_MINIMUM_ANGLE (MINIMUM_ANGLE*3)

static void rotate_by_minimum(uint32_t* x, uint32_t* y, struct image *img){
  uint32_t old_x = *x;
  uint32_t old_y = *y;
  *x = img->width - 1 - old_y;
  *y = old_x;
}

static void rotate_back(uint32_t* x, uint32_t* y, struct image *img){
  uint32_t old_x = *x;
  uint32_t old_y = *y;
  *x = old_y;
  *y = img->height - 1 - old_x;
}

static void rotate_upside_down(uint32_t* x, uint32_t* y, struct image *img){
  uint32_t old_x = *x;
  uint32_t old_y = *y;
  *x = img->width - 1 - old_x;
  *y = img->height - 1 - old_y;
}

static struct pixel rotate_pixel(uint32_t x, uint32_t y, int angle,
                                 struct image *img) {
  switch (angle) {
  case BACK_MINIMUM_ANGLE:
    rotate_back(&x, &y, img);
    break;

  case UPSIDE_DOWN_ANGLE:
    rotate_upside_down(&x, &y, img);
    break;

  case MINIMUM_ANGLE:
    rotate_by_minimum(&x, &y, img);
    break;
  }
  return img->data[y * img->width + x];
}

struct image rotate(struct image *img, int angle) {
  int proper_angle = (360 + angle) % 360;
  struct image rotated_img;

  if (angle / 90 % 2) {
    rotated_img = create_image(img->height, img->width);
  } else {
    rotated_img = create_image(img->width, img->height);
  }

  for (int y = 0; y < rotated_img.height; y++) {
    for (int x = 0; x < rotated_img.width; x++) {
      rotated_img.data[y * rotated_img.width + x] =
          rotate_pixel(x, y, proper_angle, img);
    }
  }

  return rotated_img;
}
