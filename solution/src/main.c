#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/transform.h"
#include <stdio.h>
#include <stdlib.h>

uint8_t check_angle(int angle) {
  return (((angle + 360) % 90 == 0 && angle >= -270 && angle <= 270)  ? 1 : 0);
}

int main(int argc, char **argv) {
  (void)argc;
  (void)argv; // supress 'unused parameters' warning
  const char *source = argv[1];
  const char *destination = argv[2];

  if (source == NULL) {
    fprintf(stderr,"%s\n", "No source image!");
    return 1;
  }
  if (argv[3] == NULL) {
    fprintf(stderr,"%s\n", "No angle!");
    return 1;
  }
  int angle = atoi(argv[3]);
  if (!(check_angle(angle))) {
    fprintf(stderr,"%s\n", "Wrong angle!");
    return 1;
  }

  struct image img;
  if (read_bmp(source, &img) == BMP_READER_ERROR) {
    return 1;
  }

  struct image rotated_img = rotate(&img, angle);
  if (write_bmp(destination, &rotated_img) == BMP_WRITER_ERROR) {
    free(img.data);
    free(rotated_img.data);
    return 1;
  }
  free(img.data);
  free(rotated_img.data);
  return 0;
}
