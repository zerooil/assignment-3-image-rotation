#include "utils.h"
#include <stdio.h>

enum open_status open_file( const char* name, FILE** in, const char* mode){
  *in = fopen(name, mode);

  if (*in == NULL) {
    fprintf(stderr, "%s\n", "Cannot open file!");
    fclose(*in);
    return OPEN_ERROR;
  };
  return OPEN_OK;
}
enum close_status close_file(FILE* out){
  if(fclose(out)!=0){
    fprintf(stderr, "%s\n", "Cannot close file!");
    return CLOSE_ERROR;
  };
  return CLOSE_OK;
}
