#include "bmp.h"
#include "image.h"
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>

#define TYPE 0x4D42
#define RESERVED 0
#define BIT_COUNT 24
#define PLANE 1
#define BI_SIZE 40
#define COMPRESSION 0

static long int count_padding(uint32_t width) {
  return (long)((4 - ((sizeof(struct pixel) * width) % 4)) % 4);
}

static enum pixel_read_status read_pixels(FILE *in, struct image *img) {
  long int padding = count_padding(img->width);
  if (img->data == NULL) {
    return PIXEL_ERROR;
  }
  for (uint32_t i = 0; i < img->height; ++i) {
    uint32_t result =
        fread(&img->data[img->width * i], sizeof(struct pixel), img->width, in);
    fseek(in, padding, SEEK_CUR);
    if (result != img->width) {
      return PIXEL_ERROR;
    }
  }
  return PIXEL_OK;
}

static enum pixel_read_status write_pixels(FILE *in, struct image *img) {
  long int padding = count_padding(img->width);

  for (uint32_t i = 0; i < img->height; ++i) {
    uint32_t result = fwrite(&img->data[img->width * i], sizeof(struct pixel),
                             img->width, in);
    fseek(in, padding, SEEK_CUR);
    if (result != img->width) {
      return PIXEL_ERROR;
    }
  }
  return PIXEL_OK;
}

static int is_header_valid(struct bmp_header header) {
  return header.bfType == TYPE && header.bfReserved == RESERVED &&
         header.bOffBits == sizeof(struct bmp_header) &&
         header.biSize == BI_SIZE && header.biPlanes == PLANE &&
         header.biBitCount == BIT_COUNT &&
         header.biCompression == COMPRESSION && header.biWidth >= 1 &&
         header.biHeight >= 1;
}

static enum read_status from_bmp(FILE *in, struct image *img) {
  struct bmp_header header;

  if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
    return READ_INVALID_HEADER;
  }
  if (is_header_valid(header) == 0) {
    return READ_INVALID_SIGNATURE;
  }

  *img = create_image(header.biWidth, header.biHeight);

  if (read_pixels(in, img) != PIXEL_OK) {
    return READ_INVALID_BITS;
  }

  return READ_OK;
}

static enum write_status to_bmp(FILE *out, struct image *img) {
  uint32_t file_size = sizeof(struct bmp_header) +
                       sizeof(struct pixel) * img->width * img->height;

  struct bmp_header header = {.bfType = TYPE,
                              .bfReserved = RESERVED,
                              .bOffBits = sizeof(struct bmp_header),
                              .biSize = BI_SIZE,
                              .biPlanes = PLANE,
                              .biBitCount = BIT_COUNT,
                              .biCompression = COMPRESSION,
                              .biWidth = img->width,
                              .biHeight = img->height,
                              .bfileSize = file_size};

  if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
    return WRITE_HEADER_ERROR;
  }
  if (write_pixels(out, img) != PIXEL_OK) {
    return WRITE_ERROR;
  }
  return WRITE_OK;
}

enum bmp_reader_status read_bmp(const char *name, struct image *img) {
  FILE *fileptr;
  if(open_file(name,&fileptr, "rb")==OPEN_ERROR)
    return BMP_READER_ERROR;

  enum read_status status = from_bmp(fileptr, img);
  if (status == READ_INVALID_HEADER) {
    fprintf(stderr, "%s\n", "Cannot read header!");
    close_file(fileptr);
    return BMP_READER_ERROR;
  }
  if (status == READ_INVALID_SIGNATURE) {
    fprintf(stderr, "%s\n", "Wrong header signature!");
    close_file(fileptr);
    return BMP_READER_ERROR;
  }
  if (status == READ_INVALID_BITS) {
    fprintf(stderr, "%s\n", "Error while reading pixels!");
    if (img->data!=NULL)
      free(img->data);
    close_file(fileptr);
    return BMP_READER_ERROR;
  }
  close_file(fileptr);
  return BMP_READER_OK;
}

enum bmp_writer_status write_bmp(const char *name, struct image *img) {
  FILE *fileptr;
  if(open_file(name,&fileptr, "wb")==OPEN_ERROR)
    return BMP_WRITER_ERROR;

  enum write_status status = to_bmp(fileptr, img);

  if (status == WRITE_HEADER_ERROR) {
    fprintf(stderr, "%s\n", "Cannot write header!");
    close_file(fileptr);
    return BMP_WRITER_ERROR;
  }
  close_file(fileptr);
  return BMP_WRITER_OK;
}
