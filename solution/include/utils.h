#ifndef UTILS_H_
#define UTILS_H_

#include <stdio.h>

enum open_status {
  OPEN_OK = 0,
  OPEN_ERROR
};
enum close_status {
  CLOSE_OK = 0,
  CLOSE_ERROR
};
enum open_status open_file(const char* name, FILE** in, const char* mode);
enum close_status close_file(FILE* out);
#endif
