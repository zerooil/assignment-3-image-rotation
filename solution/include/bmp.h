#include "image.h"
#include <stdio.h>

#ifndef BMP_H_
#define BMP_H_
#pragma pack(push,1)
struct bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
#pragma pack(pop)
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER
  };

enum pixel_read_status {
  PIXEL_OK = 0,
  PIXEL_ERROR,
  PIXEL_EOF
};

enum  write_status  {
  WRITE_OK = 0,
  WRITE_HEADER_ERROR,
  WRITE_ERROR
};
enum bmp_reader_status{
  BMP_READER_OK = 0,
  BMP_READER_ERROR
};
enum bmp_writer_status{
  BMP_WRITER_OK = 0,
  BMP_WRITER_ERROR
};

enum bmp_reader_status read_bmp( const char* name, struct image* img );
enum bmp_writer_status write_bmp( const char* name, struct image* img );

#endif
